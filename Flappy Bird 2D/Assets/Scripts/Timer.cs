﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour {

    public float currentTime;
    public float startingTime = 60f;

	[SerializeField] string sceneName;
	public bool continueCountdown =true;

    [SerializeField] Text countdownText;

    void Start()
    {
        currentTime = startingTime;
		continueCountdown = true;
    }

    void Update()
    {
        //Telling the timer to subtract 1 number every one second
		if (continueCountdown == true) {
			currentTime -= 1 * Time.deltaTime;
			countdownText.text = currentTime.ToString ("0");

            //When the timer becomes 0, Level 2 will load
			if (currentTime <= 0) {
				SceneManager.LoadScene (sceneName);
			}
		}
    }

}
