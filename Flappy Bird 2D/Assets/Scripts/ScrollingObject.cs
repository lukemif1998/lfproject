﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingObject : MonoBehaviour
{

    private Rigidbody2D rb2d;


    // Use this for initialization
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>(); //gets the components taht is accociated with the ground
        rb2d.velocity = new Vector2(GameControl.Instance.scrollSpeed, 0); //screen will move to the left
    }

    // Update is called once per frame
    void Update()
    {
        //checks if the game is over
        if (GameControl.Instance.isGameOver)
        {
            rb2d.velocity = Vector2.zero;
        }
    }
}