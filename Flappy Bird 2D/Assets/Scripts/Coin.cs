﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    public AudioSource CoinSound;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Player>() != null)
        {
            this.gameObject.SetActive(false);
          	CoinSound.Play();
            GameControl.Instance.Score();
            
        }

    }
}
