﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{

    public static GameControl Instance;

    public float scrollSpeed = -1.5f;
    public bool isGameOver = false;
    public int score = 0;
    public int health = 5;

    public Text scoreText;
    public Text healthText;
    public GameObject gameOvertext;


    // Use this for initialization
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

    }

    // Update is called once per frame
    void Update()
    {
        //loading the current scene
        if (isGameOver && Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void Health()
    {
        if (isGameOver) { return; }

        health--;
        healthText.text = "Health: " + health;

        if(health != 0) { return; }
        isGameOver = true;
        gameOvertext.SetActive(true);
    }

    //setting up the score
    public void Score()
    {
        if (isGameOver) { return; }

        score++;
        scoreText.text = "Score: " + score;
    }

    public void Die()
    {
        isGameOver = true;
        gameOvertext.SetActive(true);
        //GetComponent<Timer>().continueCountdown = false;
    }

}