﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : MonoBehaviour{
	public AudioSource CrashSound;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Player>() != null)
        {
            CrashSound.Play();
            GameControl.Instance.Health();
        }

    }
}